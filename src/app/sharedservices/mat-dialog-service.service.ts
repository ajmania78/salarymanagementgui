import { MatConfirmDialogComponent } from './../mat-confirm-dialog/mat-confirm-dialog.component';
import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class MatDialogService {
  

  constructor(private dialog : MatDialog) { }
  
  openDialog (){
   return this.dialog.open(MatConfirmDialogComponent, {
      width: '450px',
      panelClass :'custom-dialog-container',
      disableClose:true,
      position :{bottom : "24%", right:"50%"}
    
    
  });
}

}
