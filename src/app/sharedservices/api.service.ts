import { environment } from './../../environments/environment';
import { Salary } from './../modal/modal';
import { throwError} from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private headers; 
  private url = environment.baseurl;
   
  constructor(private http: HttpClient) { }

   getAll():Observable<Salary[]>{
    return this.http.get<Salary[]>(`${this.url}/allrecord`).pipe(
          catchError(this.errorHandler))         
  }

  addSalary(salary:Salary){
    let body = salary;
    this.headers = new HttpHeaders({'Content-type' : 'application/json'});
    if(salary.salaryId){
        return this.http.put(`${this.url}/update/`+salary.salaryId , body,this.headers)
    }else{
    return this.http.post(`${this.url}/addsalary` , body , this.headers)
  
    }
  }

deleteSalary(id: number): Observable<any>{
  return this.http.delete(`${this.url}/deletesalary/`+id, {responseType: 'text'});
}

getSalary(id: number): Observable<Salary>{
  return this.http.get<Salary>(`${this.url}/` + id);
}

  private errorHandler(error: HttpErrorResponse){
    if(error instanceof HttpErrorResponse && error.status === 404){
          console.log('Not Fount')
    }else 
    if(error instanceof HttpErrorResponse && error.status === 500){
      console.log('Internal Server Error')
    }else if(error instanceof HttpErrorResponse && error.status === 400){
      console.log('Bad Request')
    }
  
  return throwError(error.message)
}
 
}
