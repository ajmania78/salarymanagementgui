import { MatDialogService } from './../../sharedservices/mat-dialog-service.service';
import { DateValidator } from './DateValidator';
import { CustomDateAdapter } from './CustomDateAdapter';
import { ApiService } from './../../sharedservices/api.service';
import { Salary } from './../../modal/modal';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import {NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter,NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import {CustomDateParserFormatter} from './CustomDateParserFormatter';


 


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [{provide: NgbDateAdapter, useClass: CustomDateAdapter},
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatter}]
})
export class HomeComponent implements OnInit {
  salaries :Salary[];
  addSalary = new Salary();
  postData : FormGroup;
  submitted = false; 
  model: NgbDateStruct;
  DIGI_REGEXP: any = '^([1-9]{1,1}[0-9]{0,20})$';
  CASH_REGEXP: any = '^(0|[-+]?[1-9]{1,1}[0-9]{0,20})$';
  TIPS_REGEXP:any  = '^(0|[1-9][0-9]{0,20})$';
  changeButtonText :string = 'Register';
  id : boolean = false;



  constructor(private  apiService : ApiService, private formBuilder: FormBuilder,private calendar: NgbCalendar, private dialogService: MatDialogService) {
    this.postData = this.formBuilder.group({ 
      salaryId:[''],     
      dateOfWork :['', Validators.compose([Validators.required, DateValidator.dateVaidator])],  
      amountOfDay:['',[Validators.required, Validators.pattern(this.DIGI_REGEXP)]],
      cashOfDay:['',[Validators.required, Validators.pattern(this.CASH_REGEXP)]],
      tipsOfDay:['',[Validators.required, Validators.pattern(this.TIPS_REGEXP)]]  
      
      });
   }

  ngOnInit() {
    this.getAll(); 
 
  } 

  get f() { return this.postData.controls; }


  onSubmit(){
    this.submitted =true;
    if(this.postData.invalid){
      return;
    }else{
          console.log(this.postData.value)
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.postData.value))
        this.id = true;
        this.postData.reset();
    }
  }

  addData(){
    this.addSalary = this.postData.value; 
      return this.apiService.addSalary(this.addSalary).subscribe(data=>
        {        
        this.getAll();    
        if(this.id == true){
         this.changeButtonText = 'Register';
        }
        (error=>
          console.log(error)
        )})          
  }

  getAll(){
   this.apiService.getAll().subscribe((data)=>
   {
      this.salaries = data
    },
       (error)=>{console.log(error)
      
    })
  }
  
  getSalary(id: number){    
    this.apiService.getSalary(id).subscribe( data =>{  
      if(data.salaryId){
      console.log("update " + data.salaryId)
        this.changeButtonText = 'Update';
       
      } 
      
        this.addSalary = data;
        console.log(this.addSalary);      
    });
  }
  deleteSalary(id: number){
   this.apiService.deleteSalary(id).subscribe((response)=>{
      console.log(response);
      this.getAll();
    }),(error)=>{
      console.log(error);
    }
  }

  onDeleteAction(){
    this.dialogService.openDialog();
  }


}
