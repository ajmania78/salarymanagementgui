export class Salary{
        salaryId: number;
        dateOfWork: string;
        amountOfDay: number;
        cashOfDay: number;
        tipsOfDay: number;
        cashMinuTips:number;
        salaryOfMonth: number;
        semesterOnSalary: number;
        taxOnSalary: number;
        salaryAfterTax:number;
}